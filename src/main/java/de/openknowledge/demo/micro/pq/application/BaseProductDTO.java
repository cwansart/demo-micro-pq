package de.openknowledge.demo.micro.pq.application;

import de.openknowledge.demo.micro.pq.domain.Product;

public class BaseProductDTO {

  private String name;

  private long prize;

  public BaseProductDTO() {
  }

  public BaseProductDTO(final String name, final long prize) {
    this.name = name;
    this.prize = prize;
  }

  public BaseProductDTO(final Product product) {
    this.name = product.getName();
    this.prize = product.getPrize();
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public long getPrize() {
    return prize;
  }

  public void setPrize(final long prize) {
    this.prize = prize;
  }
}
