package de.openknowledge.demo.micro.pq.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TAB_PRODUCT")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "COL_ID")
  private Long id;

  @Column(name = "COL_NAME")
  private String name;

  @Column(name = "COL_PRICE")
  private long prize;

  public Product() {
  }

  public Product(final Long id, final String name, final long prize) {
    this.id = id;
    this.name = name;
    this.prize = prize;
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public long getPrize() {
    return prize;
  }

  public void setPrize(final long prize) {
    this.prize = prize;
  }

  @Override
  public String toString() {
    return "Product{" + "id=" + id + ", name='" + name + '\'' + ", prize=" + prize + '}';
  }
}
