package de.openknowledge.demo.micro.pq;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/api")
public class DemoMicroPQRestApplication extends Application {
}
