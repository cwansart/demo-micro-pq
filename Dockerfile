FROM open-liberty

USER 0

RUN apt-get update  && apt-get install wget
RUN wget https://jdbc.postgresql.org/download/postgresql-42.2.9.jar -P /opt/ol/wlp/usr/shared/resources/jdbc

USER 1001

COPY src/main/liberty/config/server.xml /config/

COPY target/demo-micro-pq.war /config/dropins/
